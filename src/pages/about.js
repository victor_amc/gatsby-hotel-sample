import React from 'react';
import Layout from '../components/Layout';
import AboutContent from '../components/AboutContent'


const About = () => {
    return (
        <Layout>
            <AboutContent/>
        </Layout>
    )
}

export default About;
