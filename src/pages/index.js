import React from 'react';
import Layout from '../components/Layout';
import ImageHotel from '../components/ImageHotel';
import HomeContent from '../components/HomeContent';
import RoomPreview from '../components/RoomPreview';
import useRooms from '../hooks/useRooms';
import {css} from '@emotion/react';
import styled from '@emotion/styled';

const RoomsUl = styled.ul`
    max-width: 1200px;
    width: 95%;
    margin: 4rem auto 0 auto;

    @media (min-width: 768px){
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        column-gap: 3rem;
    }
`;


const IndexPage = () => {

    const rooms = useRooms();

    return (
        <Layout>
            <ImageHotel/>
            <HomeContent/>

            <h2
                css = {css`
                    text-align: center;
                    margin-top: 5rem;
                    font-size: 3rem;

                `}
            >Our Rooms</h2>

            <RoomsUl>
                {rooms.map(room=>(
                    <RoomPreview
                        key = {room.id}
                        room = {room}
                    />
                ))}
            </RoomsUl>

        </Layout>
    )
}

export default IndexPage
