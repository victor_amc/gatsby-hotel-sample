
exports.createPages = async({actions, graphql,reporter}) => {
    
    //query slugs
    const result = await graphql(`
        query{
            allDatoCmsRoom{
                nodes{
                    slug
                }
            }
        }
    `);

    if (result.errors) {
        reporter.panic('No results.', result.errors);
    }

    //create pages
    const rooms = result.data.allDatoCmsRoom.nodes;

    rooms.forEach(room => {
        actions.createPage({
            path: room.slug,
            component: require.resolve('./src/components/RoomTemplate.js'),
            context: {
                slug: room.slug,
            }
        })
    });

}



