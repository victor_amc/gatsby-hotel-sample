module.exports = {
  siteMetadata: {
    title: "Gatsby Hotel",
  },
  plugins: [
    "gatsby-plugin-gatsby-cloud",
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",	
    "gatsby-transformer-sharp",
    {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `images`,
          path: `${__dirname}/src/images/`,
        },
     },
     "gatsby-plugin-emotion",
     "gatsby-plugin-react-helmet",
     {
      resolve: `gatsby-source-datocms`,
      options: {
        apiToken: `642c34c467ae08562dc7f58f3d7d4f`,
      },
    },
  ],
};
