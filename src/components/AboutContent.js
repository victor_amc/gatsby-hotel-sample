import React from 'react';
import {graphql, useStaticQuery} from 'gatsby';
import Image from 'gatsby-image';
import styled from '@emotion/styled';
import {css} from '@emotion/react';


const AboutMain = styled.main`
    padding-top: 4rem;
    max-width: 1200px;
    width: 95%;
    margin: 0 auto;

    p{
        line-height:2;
    }

    @media(min-width: 768px){
        display: grid;
        grid-template-columns: repeat()(2, 1fr); //1fr 1fr
        column-gap: 3rem;
    }
`;


const AboutContent = () => {

    const aboutContent = useStaticQuery(graphql`
        query{
            allDatoCmsPage(filter:{slug:{eq:"about"}}){
                nodes{
                    title
                    content
                    image{
                        fluid(maxWidth: 1200){
                            ...GatsbyDatoCmsFluid
                        }
                    }
                }
            }
        }
    `); //max-width from gatsby-image?

    const {title, content, image} = aboutContent.allDatoCmsPage.nodes[0];

    return (
        <>
            <h2
                css = {css`
                    text-align: center;
                    font-size: 4rem;
                    margin-top: 4rem;

                `}
            >{title}</h2>

            <AboutMain>
                <p>{content}</p>
                <Image fluid={image.fluid} />
            </AboutMain>

        </>
    )
} 

export default AboutContent